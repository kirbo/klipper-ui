# Setup

## Dokku host

```bash
dokku apps:create k1
```

## Development environment

```bash
# Clone the repository from GitLab and cd into the cloned path
git clone git@gitlab.com:kirbo/klipper-ui.git && cd "$(basename "$_" .git)"

# Add new remote for Dokku
git remote add dokku dokku@dokku.hima.devaus.eu:k1

# Add new remote for both GitLab and Dokku
git remote add all $(git remote get-url origin)
git remote set-url --add all $(git remote get-url dokku)

# Commit all modified files
git add . && git commit -am "Modifications"

# Push changes only to GitLab
git push

# Push changes only to Dokku
git push dokku

# Push changes to both GitLab and Dokku
git push all

# Oneliner to commit all modified files to both GitLab and Dokku
git add . && git commit -am "Modifications" && git push all
```

### For .gitlab-ci.yaml in the future

```bash
dokku apps:exists "${PROJECT_NAME}" 2&>/dev/null && echo "Project ${PROJECT_NAME} found already" || (echo "Project ${PROJECT_NAME} not found.. Creating project" && dokku apps:create "${PROJECT_NAME}")
```

## Helpful links
- https://dokku.com/docs/deployment/application-deployment
- https://stackoverflow.com/a/5785618
